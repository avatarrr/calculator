import React from 'react';
import {Dimensions, StyleSheet, Text, TouchableHighlight} from 'react-native';

export default class Button extends React.Component {
  render = () => {
    const styleButtonDefault = [styles.button];

    if (this.props.double) {
      styleButtonDefault.push(styles.buttonDouble);
    } else if (this.props.triple) {
      styleButtonDefault.push(styles.buttonTriple);
    } else if (this.props.operation) {
      styleButtonDefault.push(styles.operationButton);
    }

    return (
      <TouchableHighlight onPress={() => this.props.onClick(this.props.label)}>
        <Text style={styleButtonDefault}>{this.props.label}</Text>
      </TouchableHighlight>
    );
  };
}

const dimensionDefault = Dimensions.get('window').width / 4;

const styles = StyleSheet.create({
  button: {
    fontSize: 40,
    height: dimensionDefault,
    width: dimensionDefault,
    padding: 20,
    backgroundColor: '#f0f0f0',
    color: '#000',
    textAlign: 'center',
    borderWidth: 1,
    borderColor: '#888',
  },
  operationButton: {
    color: '#fff',
    backgroundColor: '#fa8231',
  },
  buttonDouble: {
    width: dimensionDefault * 2,
  },
  buttonTriple: {
    width: dimensionDefault * 3,
  },
});
