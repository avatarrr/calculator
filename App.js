import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import Button from './src/components/Button';
import Display from './src/components/Display';

const initialState = {
  displayValue: '0',
  clearDisplay: false,
  operation: null,
  values: [0, 0],
  current: 0,
};

export default class App extends React.Component {
  state = {...initialState};

  addDigit = number => {
    const clearDisplay =
      this.state.displayValue === '0' || this.state.clearDisplay;

    if (
      number === '.' &&
      !clearDisplay &&
      this.state.displayValue.includes('.')
    ) {
      return;
    }

    const currentDisplay = clearDisplay ? '' : this.state.displayValue;
    const displayValue = currentDisplay + number;

    this.setState({displayValue: displayValue, clearDisplay: false});

    if (number !== '.') {
      const newValue = Number.parseFloat(displayValue);
      const values = [...this.state.values];
      values[this.state.current] = newValue;

      this.setState({values});
    }
  };

  clearMemory = _ => this.setState({...initialState});

  setOperation = operation => {
    if (this.state.current === 0) {
      this.setState({operation, current: 1, clearDisplay: true});
    } else {
      const equals = operation === '=';
      const values = [...this.state.values];

      try {
        // eslint-disable-next-line no-eval
        values[0] = eval(`${values[0]} ${this.state.operation} ${values[1]}`);
      } catch {
        values[0] = this.state.values[0];
      }

      values[1] = 0;
      this.setState({
        displayValue: values[0].toString(),
        operation: equals ? null : operation,
        current: equals ? 0 : 1,
        clearDisplay: true,
        values,
      });
    }
  };

  render = () => {
    const dot = '.';

    const operations = {
      multiplication: '*',
      sum: '+',
      subtraction: '-',
      division: '/',
      equal: '=',
    };

    const numbers = {
      zero: '0',
      one: '1',
      two: '2',
      three: '3',
      for: '4',
      five: '5',
      six: '6',
      seven: '7',
      eight: '8',
      nine: '9',
    };

    return (
      <SafeAreaView style={styles.container}>
        <Display value={this.state.displayValue} />
        <SafeAreaView style={styles.button}>
          <Button label="AC" triple onClick={this.clearMemory} />
          <Button
            label={operations.division}
            operation
            onClick={this.setOperation}
          />
          <Button label={numbers.seven} onClick={this.addDigit} />
          <Button label={numbers.eight} onClick={this.addDigit} />
          <Button label={numbers.nine} onClick={this.addDigit} />
          <Button
            label={operations.multiplication}
            operation
            onClick={this.setOperation}
          />
          <Button label={numbers.for} onClick={this.addDigit} />
          <Button label={numbers.five} onClick={this.addDigit} />
          <Button label={numbers.six} onClick={this.addDigit} />
          <Button
            label={operations.subtraction}
            operation
            onClick={this.setOperation}
          />
          <Button label={numbers.one} onClick={this.addDigit} />
          <Button label={numbers.two} onClick={this.addDigit} />
          <Button label={numbers.three} onClick={this.addDigit} />
          <Button
            label={operations.sum}
            operation
            onClick={this.setOperation}
          />
          <Button label={numbers.zero} double onClick={this.addDigit} />
          <Button label={dot} onClick={this.addDigit} />
          <Button
            label={operations.equal}
            operation
            onClick={this.setOperation}
          />
        </SafeAreaView>
      </SafeAreaView>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  button: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});
